<?php

return [
    'notifications_title' => '你有 :nums 則通知.',
    'see_allnotifications' => '查看所有通知.',
    'profile' => '個人資訊',
    'logout' => '登出'
];