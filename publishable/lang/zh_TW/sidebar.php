<?php

return [
    'dashboard'    => '即時數據',
    'users'        => '用戶管理',
    'roles'        => '角色管理',
    'tools'        => '工具',
    'settings'     => '系統設置',
    'menu_builder' => '列表產生器',
    'read_builder' => '頁面產生器',
];