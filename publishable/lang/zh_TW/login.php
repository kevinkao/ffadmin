<?php

return [
    'title'               => '登入 - 後台',
    'form_title'          => '登入',
    'form_account_label'  => '帳號',
    'form_password_label' => '密碼',
    'form_submit_text'    => '登入',
    'remember_me'         => '記住我'
];