<?php

return [
    "decimal"        => "",
    "emptyTable"     => "No data available in table",
    "info"           =>  "顯示 _START_ 到 _END_ 共 _TOTAL_ 筆",
    "infoEmpty"      => "顯示 第 0 筆 到 第 0 筆 共 0 筆",
    "infoFiltered"   => "",
    "infoPostFix"    => "",
    "thousands"      => ",",
    "lengthMenu"     => "顯示 _MENU_ 筆",
    "loadingRecords" => "載入中...",
    "processing"     => "處理中...",
    "search"         => "搜尋:",
    "zeroRecords"    => "沒有符合資料",
    "paginate" => [
        "first"    => "最前頁",
        "last"     => "最後頁",
        "next"     => "下頁",
        "previous" => "上頁"
    ],
    "aria" => [
        "sortAscending"  => ": activate to sort column ascending",
        "sortDescending" => ": activate to sort column descending"
    ]
];