<?php

return [
    'form_field_required'           => ':field 為必填欄位',
    'form_add_success'              => '新增成功',
    'form_add_failure'              => '新增失敗',
    'form_update_success'           => '更新成功',
    'form_update_failure'           => '更新失敗',
    'form_field_password_different' => '密碼與密碼確認不同',
    
    'alert_are_you_sure_delete'     => '你確定要刪除嗎?',
    'alert_yes_delete_it'           => '確定',
    'alert_no_cancel_it'            => '取消',
    'alert_delete_success'          => '刪除成功',
    
    'error_unauthorized'            => '未經授權的的操作',
];