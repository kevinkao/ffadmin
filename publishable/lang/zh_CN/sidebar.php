<?php

return [
    'dashboard'    => '即时数据',
    'users'        => '用户管理',
    'roles'        => '角色管理',
    'tools'        => '工具',
    'settings'     => '系统设置',
];