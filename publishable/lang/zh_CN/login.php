<?php

return [
    'title'               => '登入 - 后台',
    'form_title'          => '登入',
    'form_account_label'  => '帐号',
    'form_password_label' => '密码',
    'form_submit_text'    => '登入',
    'remember_me'         => '记住我'
];