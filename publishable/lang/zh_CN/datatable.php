<?php

return [
    "decimal"        => "",
    "emptyTable"     => "No data available in table",
    "info"           =>  "显示 _START_ 至 _END_ 共 _TOTAL_ 项",
    "infoEmpty"      => "显示 第 0 项 至 第 0 项结果 共 0 项",
    "infoFiltered"   => "",
    "infoPostFix"    => "",
    "thousands"      => ",",
    "lengthMenu"     => "显示 _MENU_ 项",
    "loadingRecords" => "加载中...",
    "processing"     => "处理中...",
    "search"         => "搜寻:",
    "zeroRecords"    => "没有符合数据",
    "paginate" => [
        "first"    => "最前页",
        "last"     => "最后页",
        "next"     => "下页",
        "previous" => "上页"
    ],
    "aria" => [
        "sortAscending"  => ": activate to sort column ascending",
        "sortDescending" => ": activate to sort column descending"
    ]
];