<?php

return [
    'form_field_required'           => ':field 为必填栏位',
    'form_add_success'              => '添加成功',
    'form_add_failure'              => '添加失败',
    'form_update_success'           => '更新成功',
    'form_update_failure'           => '更新失败',
    'form_field_password_different' => '密码与密码确认不同',
    
    'alert_are_you_sure_delete'     => '你确定要删除吗?',
    'alert_yes_delete_it'           => '确定',
    'alert_no_cancel_it'            => '取消',
    'alert_delete_success'          => '删除成功',
    
    'error_unauthorized'            => '未经授权的的操作',
    
    'betflow_not_qualified'         => '不符合参加资格',
    'betflow_invalid_settings'      => '活动配置错误，请联系技术人员',
];