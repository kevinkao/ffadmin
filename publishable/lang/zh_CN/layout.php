<?php

return [
    'notifications_title' => '你有 :nums 则通知.',
    'see_allnotifications' => '查看所有通知.',
    'profile' => '个人资讯',
    'logout' => '登出'
];