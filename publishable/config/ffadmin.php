<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Path to the FFAdmin Assets
    |--------------------------------------------------------------------------
    |
    | Here you can specify the location of the ffadmin assets path
    |
    */
    'assets_path' => '/vendor/kevinkao/ffadmin/assets',

    /*
    |--------------------------------------------------------------------------
    | Prefix for admin path
    |--------------------------------------------------------------------------
    |
    | Here you can specify prefix for admin path
    |
    */
    'admin_prefix' => env('ADMIN_PREFIX', 'admin168'),

    /*
    |--------------------------------------------------------------------------
    | The super admin role list
    |--------------------------------------------------------------------------
    |
    | If you set, it will be hidden anywhere, except current user has super admin role
    |
    */
    'super_admin' => 'super_admin',

    /*
    |--------------------------------------------------------------------------
    | Default module name for locale translate prefix
    |--------------------------------------------------------------------------
    |
    | If you not set module name in this array, the role name translation of role edit page will not effect.
    | The host application's translation file path is [resources/lang/{locale}/page.php]
    |
    */
    'default_modules' => ['role', 'users', 'menus', 'settings'],
];