<?php

use Illuminate\Database\Seeder;

class FFAdminDefaultSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            ['key' => 'general.title', 'display_name' => '網站名稱', 'value' => '站名', 'type' => 'text', 'group' => 'General'],
            ['key' => 'general.description', 'display_name' => '網站描述', 'value' => '網站描述', 'type' => 'text', 'group' => 'General'],
        ]);
    }
}