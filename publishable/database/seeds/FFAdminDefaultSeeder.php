<?php

use Illuminate\Database\Seeder;

class FFAdminDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FFAdminDefaultUserSeeder::class);
        $this->call(FFAdminDefaultMenuSeeder::class);
        $this->call(FFAdminDefaultSettingsSeeder::class);
    }
}