<?php

use Illuminate\Database\Seeder;

class FFAdminDefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminId = DB::table('users')->insertGetId([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ]);
        $superAdminId = DB::table('roles')->insertGetId([
            'name' => 'super_admin',
            'display_name' => '超級管理者'
        ]);
        $adminRoleId = DB::table('roles')->insertGetId([
            'name' => 'admin',
            'display_name' => '管理者'
        ]);
        DB::table('roles')->insertGetId([
            'name' => 'user',
            'display_name' => '普通用戶'
        ]);
        DB::table('user_roles')->insert([
            'user_id' => $adminId,
            'role_id' => $superAdminId
        ]);
        DB::table('permission_role')->insert([
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'role_add' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'role_delete' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'role_edit' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'role_browse' ])],

            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'users_add' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'users_delete' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'users_edit' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'users_browse' ])],

            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'menus_add' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'menus_delete' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'menus_edit' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'menus_browse' ])],

            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'settings_add' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'settings_delete' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'settings_edit' ])],
            ['role_id' => $adminRoleId, 'permission_id' => DB::table('permissions')->insertGetId([ 'key' => 'settings_browse' ])],
        ]);
    }
}
