<?php

use Illuminate\Database\Seeder;

class FFAdminDefaultMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_items')->insert([
            [
                'title' => '網站狀態',
                'slug' => 'dashboard',
                'route' => 'ffadmin.dashboard',
                'icon_class' => 'fa-dashboard',
                'order' => '1',
            ],
            [
                'title' => '角色管理',
                'slug' => 'role',
                'route' => 'ffadmin.role.index',
                'icon_class' => 'fa-lock',
                'order' => '2',
            ],
            [
                'title' => '用戶管理',
                'slug' => 'users',
                'route' => 'ffadmin.users.index',
                'icon_class' => 'fa-users',
                'order' => '3',
            ],
            [
                'title' => '列表管理',
                'slug' => 'menus',
                'route' => 'ffadmin.menus.index',
                'icon_class' => 'fa-list-ul',
                'order' => '4',
            ],
            [
                'title' => '系統設置',
                'slug' => 'settings',
                'route' => 'ffadmin.settings.index',
                'icon_class' => 'fa-cogs',
                'order' => '5',
            ],
        ]);
    }
}