(function ($) {
    $.extend(validator, {
        'required': function (input) {
            return validator.matches(input, /.+/);
        }
    });
    $('.needs-validation').on('submit', function () {
        var invalidCount = 0;
        $('[verify]', this).each(function () {
            var $self = $(this);
            var rules = $self.attr('verify').split('|');           
            var value = $self.is('input') ? $self.val() : this.value;
            $self.removeClass('is-invalid');
            $.each(rules, function (_, ruleName) {
                if (validator[ruleName].apply(validator, [value]) === false) {
                    $self.addClass('is-invalid');
                    invalidCount++;
                    return true;
                }
            });
        });

        if (invalidCount > 0) {
            return false;
        }
    });

})(jQuery);