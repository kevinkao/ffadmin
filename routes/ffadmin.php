<?php

$prefix = config('ffadmin.admin_prefix');

Route::group(['as' => 'ffadmin.', 'prefix' => $prefix, 'middleware' => ['web']], function () {

    $namespacePrefix = '\\KevinKao\\Admin\\Http\\Controllers';

    Route::get('login', "{$namespacePrefix}\AuthController@index")->name('login');
    Route::post('login', "{$namespacePrefix}\AuthController@login")->name('post.login');

    Route::group(['middleware' => ['admin.user', 'admin.locale']], function () use ($namespacePrefix) {

        Route::get('/', function () {
            return redirect()->route('ffadmin.dashboard');
        });

        Route::post('logout', "{$namespacePrefix}\AuthController@logout")->name('post.logout');
        Route::view('/dashboard', 'ffadmin::dashboard')->name('dashboard');

        Route::get('/roles', "{$namespacePrefix}\RoleController@index")->name('role.index');
        Route::get('/roles/datatable', "{$namespacePrefix}\RoleController@dataTable")->name('role.datatable');
        Route::get('/roles/{id}/edit', "{$namespacePrefix}\RoleController@edit")->name('role.edit');
        Route::get('/roles/create', "{$namespacePrefix}\RoleController@show")->name('role.create');
        Route::post('/roles', "{$namespacePrefix}\RoleController@store")->name('role.store');
        Route::post('/roles/{id}', "{$namespacePrefix}\RoleController@update")->name('role.update');
        Route::delete('/roles/{id}', "{$namespacePrefix}\RoleController@destroy")->name('role.destroy');

        Route::resource('menus', "{$namespacePrefix}\MenuController");

        Route::post('/users/datatable', "{$namespacePrefix}\UserController@dataTable")->name('users.datatable');
        Route::post('/users/{user}', "{$namespacePrefix}\UserController@update")->name('users.update');
        Route::resource('users', "{$namespacePrefix}\UserController");

        Route::post('/settings/update', "{$namespacePrefix}\SettingsController@updateAll")->name('settings.update.all');
        Route::resource('settings', "{$namespacePrefix}\SettingsController");

    });

});