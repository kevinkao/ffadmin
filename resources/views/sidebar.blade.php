<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <img src="https://ui-avatars.com/api/?size=32&name={{ auth()->user()->name }}" alt="" class="app-sidebar__user-avatar">
        <div>
            <p class="app-sidebar__user-name">{{ auth()->user()->name }}</p>
            <p class="app-sidebar__user-designation">{{ auth()->user()->email }}</p>
        </div>
    </div>
    <ul class="app-menu">
        @foreach ($items as $item)
            @can('browse', $item->slug)
                <li>
                    <a
                        class="app-menu__item @if (isset($item->route) && Route::has($item->route)) {{Request::is(substr(route($item->route, [], false), 1) . '*') ? 'active' : ''}} @endif"
                        href="{{ isset($item->route) && Route::has($item->route) ? route($item->route) : $item->url }}"
                        target="{{ $item->target }}">
                        <i class="app-menu__icon fa {{ $item->icon_class }}"></i>
                        <span class="app-menu__label">{{ $item->title }}</span>
                    </a>
                </li>
            @endcan
        @endforeach
    </ul>
</aside>