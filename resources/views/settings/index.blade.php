@extends('ffadmin::layout')

@section('page_title', __('ffadmin::page.settings_title'))

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@section('content')
    <div class="modal fade" id="doc-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('ffadmin::page.settings_add_modal_rules')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>@lang('ffadmin::page.formfield_type_select')</h5>
                    <code>{"options": [{"name": "name", "value": "value"}]}</code>
                    <h5>@lang('ffadmin::page.formfield_type_input_radio')</h5>
                    <code>{"options": [{"title": "name", "value": "value"}]}</code>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('ffadmin::page.close_btn')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="app-title">
        <div>
            <h1 style="display: inline-block; vertical-align: middle">
                <i class="fa fa-cogs"></i> {{ __('ffadmin::page.settings_title') }}
            </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a>{{ __('ffadmin::page.settings_title') }}</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <form method="POST" action="{{ route('ffadmin.settings.update.all') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="tile-body">
                        <ul class="nav nav-tabs">
                            @foreach($settings as $groupName => $item)
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#{{ $groupName }}">{{ $groupName }}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content" id="settings-content">
                            @foreach($settings as $groupName => $items)
                                <div class="tab-pane fade" id="{{ $groupName }}" style="padding: 10px 0;">
                                    @foreach ($items as $setting)
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="{{ $setting->key }}" style="font-size: 16px; width: 100%; position: relative;">
                                                    {{ $setting->display_name }} <code style="margin-left: 4px">setting('{{ $setting->key }}')</code>
                                                    @can('delete', 'settings')
                                                        <div class="action" style="position: absolute; top: 0; right: 10px">
                                                            <a
                                                                href="javascript:void(0)"
                                                                class="delete"
                                                                data-url="{{ route('ffadmin.settings.destroy', $setting->id) }}">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    @endcan
                                                </label>
                                                @if ($setting->type === 'text')
                                                    <input id="{{ $setting->key }}" name="{{ $setting->key }}" class="form-control" type="text" value="{{ $setting->value }}">
                                                @elseif ($setting->type === 'textarea')
                                                    <textarea class="form-control" name="{{ $setting->key }}" id="{{ $setting->key }}" cols="30" rows="10">{{ isset($setting->value) ? $setting->value : '' }}</textarea>
                                                @elseif ($setting->type === 'datetime_picker')
                                                    <input type="text" name="{{ $setting->key }}" value="{{ $setting->value }}" class="datetime-picker form-control">
                                                @elseif ($setting->type === 'select')
                                                    <select class="form-control" name="{{ $setting->key }}" id="{{ $setting->key }}">
                                                        @foreach ($setting->details->options as $option)
                                                            <option
                                                                {{ $option->value == $setting->value ? 'selected' : '' }}
                                                                value="{{ $option->value }}">
                                                                {{ $option->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @elseif ($setting->type === 'input_radio')
                                                    @foreach ($setting->details->options as $option)
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input
                                                                    class="form-check-input"
                                                                    type="radio"
                                                                    name="{{ $setting->key }}"
                                                                    value="{{ $option->value }}"
                                                                    {{ $option->value == $setting->value ? 'checked' : '' }}>
                                                                {{ $option->title }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                @elseif ($setting->type === 'image_upload')
                                                    <img src="{{ '/storage/' . $setting->value }}" alt="" style="max-width: 50%;margin: 2px 0;">
                                                    <input class="form-control-file" name="{{ $setting->key}}" type="file" aria-describedby="fileHelp">
                                                @elseif ($setting->type === 'editor')
                                                    <textarea class="tinymce-editor" name="{{ $setting->key }}">{{ $setting->value }}</textarea>
                                                @endif

                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @can('edit', 'settings')
                        <div class="tile-footer">
                            <button class="btn btn-primary">@lang('ffadmin::page.form_submit_settings_btn')</button>
                        </div>
                    @endcan
                </form>
            </div>
        </div>
        
        @can('add', 'settings')
            <div id="add-settings-panel" class="col-md-12">
                <div class="tile">
                    <form class="needs-validation">
                    <div class="tile-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="display_name">@lang('ffadmin::page.settings_col_display_name')</label>
                                    <input
                                        verify="required"
                                        id="display_name"
                                        name="display_name"
                                        class="form-control"
                                        type="text"
                                        placeholder="@lang('ffadmin::page.settings_placeholder_display_name')">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="key_1">@lang('ffadmin::page.settings_col_key')</label>
                                    <input
                                        verify="required"
                                        id="key_1"
                                        name="key"
                                        class="form-control"
                                        type="text"
                                        placeholder="@lang('ffadmin::page.settings_placeholder_key')">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="type_1">@lang('ffadmin::page.settings_col_type')</label>
                                    <select verify="required" name="type" id="type_1" class="form-control">
                                        <option value="">@lang('ffadmin::page.settings_col_select_option_type')</option>
                                        @foreach ($availableType as $type)
                                            <option value="{{ $type }}">{{ __('ffadmin::page.formfield_type_' . $type) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="group_1">@lang('ffadmin::page.settings_col_group')</label>
                                    <select verify="required" name="group" id="group_1" class="form-control">
                                        <option value="">@lang('ffadmin::page.settings_col_select_option_group')</option>
                                        @foreach ($settings as $groupName => $item)
                                            <option value="{{ $groupName }}">{{ $groupName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 editor-wrapper">
                                <div class="form-group">
                                    <label style="position: relative;">
                                        @lang('ffadmin::page.settings_add_details_tip')
                                        <div style="position: absolute; top: 0; right: -30px;">
                                            <a href="javascript:void(0)"><i class="fa fa-chevron-circle-down"></i></a>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#doc-modal">
                                                <i class="fa fa-question-circle"></i>
                                            </a>
                                        </div>
                                    </label>
                                    <textarea name="details" id="editor_textarea" style="display: none;"></textarea>
                                    <div id="editor" style="display: none; width: 100%; height: 200px; border: 2px solid #ced4da;border-radius: 4px"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tile-footer">
                        <button id="add-new-settings" class="btn btn-primary">@lang('ffadmin::page.form_submit_new_settings_btn')</button>
                    </div>
                    </form>
                </div>
            </div>
        @endcan

    </div>
@endsection


@push('scripts')
<script src="{{ ffadmin_asset('js/plugins/validator.min.js') }}"></script>
<script src="{{ ffadmin_asset('js/validation.js') }}"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/bootstrap-notify.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/zh.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.1/ace.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.9.2/tinymce.min.js"></script>
@if (App::isLocale('zh_CN'))
<script src="{{ ffadmin_asset('js/tinymce/lang/zh_cn.js') }}"></script>
@endif

@if (Session::has('message'))
<script>
    $.notify({
        message: '{{ Session::get('message') }}',
    }, {
        type: '{{ Session::get('alert-type') }}'
    });
</script>
@endif
<script>
    $('.datetime-picker').flatpickr({
        enableTime: true,
        dateFormat: 'Y-m-d H:i',
        time_24hr: true,
        locale: 'zh'
    });
    $('.nav-link:first').addClass('active');
    $('.tab-pane:first').addClass('active show')
    $('#add-settings-panel').on('submit', 'form', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            url: '{{ route('ffadmin.settings.store') }}',
            type: 'post',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (resp) {
            location.reload();
        });
    });
    $('#settings-content').on('click', '.delete', function () {
        var self = this;
        swal({
            title: '{{ __('ffadmin::message.alert_are_you_sure_delete') }}',
            confirmButtonText: '{{ __('ffadmin::message.alert_yes_delete_it') }}',
            cancelButtonText: '{{ __('ffadmin::message.alert_no_cancel_it') }}',
            type: 'warning',
            showCancelButton: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: $(self).data('url'),
                    type: 'delete',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function (resp) {
                    location.reload();
                });
            }
        });
    });

    $('.editor-wrapper')
        .on('click', '.fa-chevron-circle-up', function () {
            $(this).removeClass('fa-chevron-circle-up').addClass('fa-chevron-circle-down');
            $('#editor').hide();
        })
        .on('click', '.fa-chevron-circle-down', function () {
            $(this).removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-up');
            $('#editor').show();
        });

    tinymce.init({
        selector:'.tinymce-editor',
        plugins: 'table link preview textcolor colorpicker',
        // toolbar1: "forecolor backcolor",
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | table | forecolor backcolor | fontsizeselect",
        height: '200'
    });

    var editor = ace.edit('editor');
    editor.getSession().setMode('ace/mode/json');
    editor.on('change', function (event, el) {
        editor = ace.edit(el.container.id);
        $('#' + el.container.id + '_textarea').val(editor.getValue());
    });

</script>
@endpush