@extends('ffadmin::layout')

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css">
@endpush

@section('page_title', __('ffadmin::page.menus_title'))

@section('content')
    <div class="app-title">
        <div>
            <h1 style="display: inline-block; vertical-align: middle">
                <i class="fa fa-list-ul"></i> {{ __('ffadmin::page.menus_title') }}
            </h1>
            <a href="javascript:void(0)" class="btn btn-primary btn-sm" style="margin-left: 10px" data-toggle="modal" data-target="#edit-add-modal">
                <i class="fa fa-plus"></i> {{__('ffadmin::page.add_btn') }}
            </a>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a>{{ __('ffadmin::page.menus_title') }}</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div id="edit-add-modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <form class="needs-validation" action="">
                                <div class="overlay" style="z-index: 500">
                                    <div class="m-loader mr-4">
                                        <svg class="m-circular" viewBox="25 25 50 50">
                                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"/>
                                        </svg>
                                    </div>
                                    <h3 class="l-text">@lang('ffadmin::page.overlay_loading_text')</h3>
                                </div>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-add-modal-label"></h5>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="title">@lang('ffadmin::page.menus_col_title')</label>
                                        <input verify="required" placeholder="@lang('ffadmin::page.form_col_required')" id="title" name="title" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="url">@lang('ffadmin::page.menus_col_url')</label>
                                        <input placeholder="@lang('ffadmin::page.form_col_optional')" id="url" name="url" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="icon_class">@lang('ffadmin::page.menus_col_icon')(<a href="https://fontawesome.com/v4.7.0/" target="_blank">Font Awesome</a>)</label>
                                        <input placeholder="@lang('ffadmin::page.form_col_optional')" id="icon_class" name="icon_class" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="route">@lang('ffadmin::page.menus_col_route')</label>
                                        <input placeholder="@lang('ffadmin::page.form_col_optional')" id="route" name="route" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="edit-add-modal-submit" class="btn btn-primary">@lang('ffadmin::page.form_submit_btn')</button>
                                    <button class="btn btn-secondary" data-dismiss="modal">@lang('ffadmin::page.form_cancel_btn')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tile-body">
                    <div class="dd">
                        <ol class="dd-list">
                            @foreach ($menuItems as $item)
                            <li class="dd-item" data-id="1">
                                <div class="pull-right dd-actions">
                                    <a
                                        href="javascript:void(0)"
                                        data-src="{{ route('ffadmin.menus.show', $item->id) }}"
                                        data-toggle="modal"
                                        data-target="#edit-add-modal"
                                        class="btn btn-info btn-sm"
                                        title="{{ __('ffadmin::page.edit_btn') }}">
                                        <i style="margin-right: 0" class="fa fa-edit"></i>
                                    </a>
                                    <a
                                        data-src="{{ route('ffadmin.menus.destroy', $item->id) }}"
                                        href="javascript:void(0)"
                                        class="btn btn-danger btn-sm delete"
                                        title="{{ __('ffadmin::page.delete_btn') }}">
                                        <i style="margin-right: 0" class="fa fa-trash"></i>
                                    </a>
                                </div>
                                <div class="dd-handle">
                                    {{ $item->title }}
                                    <span style="margin-left: 10px; font-size: 12px">
                                        {{ isset($item->route) && Route::has($item->route) ? route($item->route, [], false) : $item->url }}
                                    </span>
                                </div>
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/bootstrap-notify.min.js') }}"></script>
<script src="{{ ffadmin_asset('js/plugins/validator.min.js') }}"></script>
<script src="{{ ffadmin_asset('js/validation.js') }}"></script>
@if (Session::has('message'))
<script>
    $.notify({
        message: '{{ Session::get('message') }}',
    }, {
        type: '{{ Session::get('alert-type') }}'
    });
</script>
@endif
<script>
    $('.dd').nestable({ /* config options */ });
    $('.dd-list').on('click', '.btn.delete', function () {
        var self = this;
        swal({
            title: '{{ __('ffadmin::message.alert_are_you_sure_delete') }}',
            confirmButtonText: '{{ __('ffadmin::message.alert_yes_delete_it') }}',
            cancelButtonText: '{{ __('ffadmin::message.alert_no_cancel_it') }}',
            type: 'warning',
            showCancelButton: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: $(self).data('src'),
                    type: 'delete',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function (resp) {
                    location.reload();
                });
            }
        });
    })
    $('#edit-add-modal').on('show.bs.modal', function (event) {
        var $button = $(event.relatedTarget);
        var src = $button.data('src');
        var $allInput = $('input', 'form').val('');
        $('form').attr('action', '');

        if (src) {
            // Edit
            $('#edit-add-modal-label').html('{{ __('ffadmin::page.menus_update') }}');
            $('.overlay').show();
            $.get(src).done(function (resp) {
                $('form').attr('action', resp.update_url);
                $allInput.each(function () {
                    $(this).val(resp[this.name] || '');
                });
            }).always(function () {
                $('.overlay').hide();
            });
        } else {
            // Add
            $('.overlay').hide();
            $('#edit-add-modal-label').html('{{ __('ffadmin::page.menus_add') }}');
        }
    }).on('submit', 'form', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var data = $(this).serialize();
        $('.overlay').show();
        if (/.+/.test(action)) {
            // edit
            $.ajax({
                url: action,
                type: 'put',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (resp) {
                $('#edit-add-modal').modal('hide');
                location.reload();
            }).always(function () {
                $('.overlay').hide();
            });
        } else {
            // add
            $.ajax({
                url: '{{ route('ffadmin.menus.store') }}',
                type: 'post',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (resp) {
                $('#edit-add-modal').modal('hide');
                location.reload();
            }).always(function () {
                $('.overlay').hide();
            });
        }
    });
</script>
@endpush