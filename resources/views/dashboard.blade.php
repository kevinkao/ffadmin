
@extends('ffadmin::layout')

@section('page_title', __('ffadmin::page.dashboard_title'))

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> {{ __('ffadmin::page.dashboard_title') }}</h1>
            <p></p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item">{{ __('ffadmin::page.dashboard_title') }}</li>
        </ul>
    </div> 
@endsection