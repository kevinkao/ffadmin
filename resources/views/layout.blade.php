<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="{{ setting('general.description') }}">
    <title>@yield('page_title', setting('general.title') .' - ' . setting('general.description'))</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ ffadmin_asset('css/app.css') }}">
    @stack('styles')
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header">
      <a class="app-header__logo" href="{{ route('ffadmin.dashboard') }}">Vali</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!--Notification Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i></a>
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">{{ __('ffadmin::layout.notifications_title', ['nums' => 0]) }}</li>
            <li class="app-notification__footer"><a href="#">{{ __('ffadmin::layout.see_allnotifications') }}</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="{{ route('ffadmin.users.edit', Auth::user()->id) }}">
              <i class="fa fa-user fa-lg"></i> {{ __('ffadmin::layout.profile') }}</a>
            </li>
            <li>
              <a class="dropdown-item" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out fa-lg"></i> {{ __('ffadmin::layout.logout') }}</a>
                <form id="logout-form" method="post" action="{{ route('ffadmin.post.logout') }}" style="display: none">
                  {{ csrf_field() }}
                </form>
            </li>
          </ul>
        </li>
      </ul>
    </header>
    
    {!! FFAdmin::renderMenu() !!}

    <main class="app-content">
      @yield('content')
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="{{ ffadmin_asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ ffadmin_asset('js/popper.min.js') }}"></script>
    <script src="{{ ffadmin_asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ ffadmin_asset('js/main.js') }}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{ ffadmin_asset('js/plugins/pace.min.js') }}"></script>
    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    @stack('scripts')
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
      }
    </script>
  </body>
</html>