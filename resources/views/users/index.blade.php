@extends('ffadmin::layout')

@section('page_title', __('ffadmin::page.users_title'))

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    <div class="app-title">
        <div>
            <h1 style="display: inline-block; vertical-align: middle">
                <i class="fa fa-lock"></i> {{ __('ffadmin::page.users_title') }}
            </h1>
            @can('add', 'users')
                <a href="{{ route('ffadmin.users.create') }}" class="btn btn-primary btn-sm" style="margin-left: 10px">
                    <i class="fa fa-plus"></i> {{__('ffadmin::page.add_btn') }}
                </a>
            @endcan
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a>{{ __('ffadmin::page.users_title') }}</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="data-table">
                        <thead>
                            <tr>
                                <th>{{ __('ffadmin::page.users_col_name') }}</th>
                                <th>{{ __('ffadmin::page.users_col_email') }}</th>
                                <th>{{ __('ffadmin::page.users_col_created_at') }}</th>
                                <th>{{ __('ffadmin::page.users_col_action') }}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/bootstrap-notify.min.js') }}"></script>
@if (Session::has('message'))
<script>
    $.notify({
        message: '{{ Session::get('message') }}',
    }, {
        type: '{{ Session::get('alert-type') }}'
    });
</script>
@endif
<script>
    var $dataTable = $('#data-table').DataTable({
        "language": {
            "info": "{{ __('ffadmin::datatable.info') }}",
            "infoEmpty": "{{ __('ffadmin::datatable.infoEmpty') }}",
            "infoFiltered": "{{ __('ffadmin::datatable.infoFiltered') }}",
            "lengthMenu": "{{ __('ffadmin::datatable.lengthMenu') }}",
            "search": "{{ __('ffadmin::datatable.search') }}",
            "zeroRecords": "{{ __('ffadmin::datatable.zeroRecords') }}",
            "paginate": {
                "first": "{{ __('ffadmin::datatable.paginate.first') }}",
                "last": "{{ __('ffadmin::datatable.paginate.last') }}",
                "next": "{{ __('ffadmin::datatable.paginate.next') }}",
                "previous": "{{ __('ffadmin::datatable.paginate.previous') }}"
            }
        },
        "autoWidth": false,
        "serverSide": true,
        "ajax": {
            url: '{{ route('ffadmin.users.datatable') }}',
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "created_at" },
            { "data": "actions" },
        ]
    }).on('click', 'a.btn.delete', function () {
        var self = this;
        swal({
            title: '{{ __('ffadmin::message.alert_are_you_sure_delete') }}',
            confirmButtonText: '{{ __('ffadmin::message.alert_yes_delete_it') }}',
            cancelButtonText: '{{ __('ffadmin::message.alert_no_cancel_it') }}',
            type: 'warning',
            showCancelButton: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: $(self).data('url'),
                    type: 'delete',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function (resp) {
                    console.log(resp);
                    $dataTable.ajax.reload();
                   $.notify({ message: resp.message, }, { type: 'success' }); 
                });
            }
        });
    });
</script>
@endpush