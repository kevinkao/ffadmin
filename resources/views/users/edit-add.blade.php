
@extends('ffadmin::layout')

@section('page_title', __('ffadmin::page.users_' . (isset($user) ? 'edit' : 'add')))

@section('content')
    <div class="app-title">
        <div>
            <h1 style="display: inline-block; vertical-align: middle">
                <i class="fa fa-lock"></i> {{ __('ffadmin::page.users_' . (isset($user) ? 'edit' : 'add')) }}
            </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{ route('ffadmin.users.index') }}">{{ __('ffadmin::page.users_title') }}</a></li>
            <li class="breadcrumb-item"><a>{{ __('ffadmin::page.users_' . (isset($user) ? 'edit' : 'add')) }}</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <form
                    class="needs-validation"
                    method="POST"
                    action="{{ isset($user) ? route('ffadmin.users.update', $user->id) : route('ffadmin.users.store') }}">
                    <div class="tile-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">{{ __('ffadmin::page.users_col_name') }}</label>
                                    <input verify="required" value="{{ (isset($user) ? $user->name : old('name')) }}" id="name" name="name" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email">{{ __('ffadmin::page.users_col_email') }}</label>
                                    <input verify="required|isEmail" value="{{ (isset($user) ? $user->email : old('email')) }}" id="email" name="email" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="password">{{ __('ffadmin::page.users_col_password') }}</label>
                                    <input value="" id="password" name="password" type="password" class="form-control">
                                    @if (isset($user))<small class="form-text text-muted">{{ __('ffadmin::page.users_tip_keep_empty_not_update') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="password_confirmation">{{ __('ffadmin::page.users_col_password_confirmation') }}</label>
                                    <input value="" id="password_confirmation" name="password_confirmation" type="password" class="form-control">
                                    <small class="form-text text-muted">{{ __('ffadmin::page.users_tip_repeat_password') }}</small>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="role_select">{{ __('ffadmin::page.users_col_role') }}</label>
                                    <select class="form-control" name="role_select[]" id="role_select" multiple>
                                        @foreach ($roles as $role)
                                            <option
                                                {{ isset($user) && $user->roles->contains($role) ? 'selected' : '' }}
                                                value="{{ $role->id }}">
                                                {{ $role->display_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="locale">@lang('ffadmin::page.users_col_locale')</label>
                                    <select name="locale" id="locale" class="form-control">
                                        @foreach ($locale as $item)
                                            <option
                                                value="{{ $item }}"
                                                {{ (isset($user) && $user->locale === $item) ? 'selected' : '' }}>
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">@lang('ffadmin::page.form_submit_btn')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ ffadmin_asset('js/plugins/validator.min.js') }}"></script>
<script src="{{ ffadmin_asset('js/validation.js') }}"></script>
@if (Session::has('message'))
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/bootstrap-notify.min.js') }}"></script>
<script>
    $.notify({
        message: '{{ Session::get('message') }} {{ Session::has('detail') ? '('.Session::get('detail').')' : '' }}',
    }, {
        type: '{{ Session::get('alert-type') }}'
    });
</script>
@endif
<script></script>
@endpush