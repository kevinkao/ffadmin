
@extends('ffadmin::layout')

@section('content')
    <div class="app-title">
        <div>
            <h1 style="display: inline-block; vertical-align: middle">
                <i class="fa fa-lock"></i> {{ __('ffadmin::page.role_' . (isset($role) ? 'edit' : 'add')) }}
            </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{ route('ffadmin.role.index') }}">{{ __('ffadmin::page.role_title') }}</a></li>
            <li class="breadcrumb-item"><a>{{ __('ffadmin::page.role_' . (isset($role) ? 'edit' : 'add')) }}</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <form
                    method="POST"
                    action="{{ isset($role) ? route('ffadmin.role.update', $role->id) : route('ffadmin.role.store') }}">
                    <div class="tile-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">{{ __('ffadmin::page.role_field_name') }}</label>
                                    <input value="{{ (isset($role) ? $role->name : old('name')) }}" id="name" name="name" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="display_name">{{ __('ffadmin::page.role_field_display_name') }}</label>
                                    <input value="{{ (isset($role) ? $role->display_name : old('display_name')) }}" id="display_name" name="display_name" type="text" class="form-control">
                                </div>
                            </div>
                            @foreach ($permissionGroup as $group => $permissions)
                                <div class="col-6 col-lg-3">
                                    <label class="custom-control" for="{{ $group }}-check-all">
                                        {{-- <input id="{{ $group }}-check-all" type="checkbox" class="form-check-input check-all"> --}}
                                        @if (in_array($group, config('ffadmin.default_modules')))
                                            <span>{{ __('ffadmin::page.' . $group . '_title') }}</span>
                                        @else
                                            <span>{{ __('page.' . $group . '_title') }}</span>
                                        @endif
                                    </label>
                                    @foreach ($permissions as $key => $permission)
                                        <label class="custom-control" for="{{ $permission->id }}" style="margin-left: 30px">
                                            <input
                                                name="permissions[]"
                                                id="{{ $permission->id }}"
                                                value="{{ $permission->id }}"
                                                type="checkbox"
                                                class="form-check-input" 
                                                {{ (isset($ownPermissions) && $ownPermissions->contains('id', $permission->id)) ? 'checked' : '' }}/>
                                            <span>{{ __('ffadmin::page.rbac_' . $key) }}</span>
                                        </label>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">@lang('ffadmin::page.form_submit_btn')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@if (Session::has('message'))
<script type="text/javascript" src="{{ ffadmin_asset('js/plugins/bootstrap-notify.min.js') }}"></script>
<script>
    $.notify({
        message: '{{ Session::get('message') }}',
    }, {
        type: '{{ Session::get('alert-type') }}'
    });
</script>
@endif
@endpush