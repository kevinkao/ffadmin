
@extends('ffadmin::layout')

@section('content')
    <div class="page-error tile">
        <h1>
            <i class="fa fa-exclamation-circle"></i>
            @lang('ffadmin::message.error_unauthorized')
        </h1>
        <p></p>
        <p><a href="javascript:window.history.back()" class="btn btn-primary">@lang('ffadmin::page.back_btn')</a></p>
    </div>
@endsection