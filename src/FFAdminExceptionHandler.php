<?php

namespace KevinKao\Admin;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Access\AuthorizationException;

class FFAdminExceptionHandler extends ExceptionHandler
{
    public function render($request, Exception $exception)
    {
        if ($request->is(config('ffadmin.admin_prefix') . '*')) {

            if ($exception instanceof AuthorizationException) {
                if ($request->wantsJson()) {
                    $resp = [
                        'message' => __('ffadmin::message.error_unauthorized'),
                        'alert-type' => 'warning'
                    ];
                    return response()->json($resp)->with($resp);
                }
                return response()->view('ffadmin::errors.403');
            }

        }
        return parent::render($request, $exception);
    }
}