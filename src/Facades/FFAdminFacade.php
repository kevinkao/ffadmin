<?php

namespace KevinKao\Admin\Facades;

use Illuminate\Support\Facades\Facade;

class FFAdminFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ffadmin';
    }
}
