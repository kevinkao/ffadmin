<?php

namespace KevinKao\Admin;

use KevinKao\Admin\Models\MenuItem;
use KevinKao\Admin\Models\Settings;
use Route;

class FFAdmin
{
    public $settingCache = null;

    public function route ($closure = null)
    {
        require_once(__DIR__ . '/../routes/ffadmin.php');
        if ($closure !== null) {
            $this->routeAdmin($closure);
        }
    }

    public function routeAdmin ($closure)
    {
        Route::group([
            'as' => 'ffadmin.',
            'prefix' => config('ffadmin.admin_prefix'),
            'middleware' => ['admin.user', 'admin.locale']
        ], function () use ($closure) {
            $closure();
        });
    }

    public function renderMenu ()
    {
        $items = MenuItem::orderBy('order')->get();
        return view('ffadmin::sidebar', compact('items'));
    }

    public function setting ($key, $default = null)
    {
        if (is_null($this->settingCache)) {
            foreach (Settings::all() as $setting) {
                $keys = explode('.', $setting->key);
                $this->settingCache[$keys[0]][$keys[1]] = $setting->value;
            }
        }

        $parts = explode('.', $key);
        if (count($parts) === 2) {
            return isset($this->settingCache[$parts[0]][$parts[1]]) ?
                        $this->settingCache[$parts[0]][$parts[1]] :
                        $default;
        } else {
            return @$this->settingCache[$parts[0]] ?: $default;
        }
    }
}