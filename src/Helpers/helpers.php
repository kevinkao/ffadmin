<?php

if (!function_exists('ffadmin_asset')) {
    function ffadmin_asset($path, $secure = null)
    {
        return asset(config('ffadmin.assets_path').'/'.$path, $secure);
    }
}

if (!function_exists('setting')) {
    function setting($key, $default = null)
    {
        return FFAdmin::setting($key, $default);
    }
}