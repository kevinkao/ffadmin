<?php

namespace KevinKao\Admin\Http\Controllers;

use Illuminate\Http\Request;
use KevinKao\Admin\Models\Role;
use KevinKao\Admin\Models\Permission;
use Log;
use Validator;
use DB;

class RoleController extends Controller
{
    public function index ()
    {
        $this->authorize('browse', 'role');
        return view('ffadmin::roles.index');
    }

    public function dataTable (Request $request)
    {
        $this->authorize('browse', 'role');

        $columns = ['name', 'display_name'];
        $isSuperAdmin = auth()->user()->isSuperAdmin();

        $totalRows = $isSuperAdmin ? Role::count() : Role::whereNotIn('name', [config('ffadmin.super_admin')])->count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if (empty($request->input('search.value'))) {
            $rolesQuery = Role::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

            if (! $isSuperAdmin) {
                $rolesQuery->exceptSuperAdmin();
            }
            $roles = $rolesQuery->get();
        } else {
            $search = $request->input('search.value');

            $roles = Role::where('name', 'LIKE', "{$search}%")
                            ->orWhere('display_name', 'LIKE', "{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            if (! $isSuperAdmin) {
                $roles = $roles->whereNotIn('name', [config('ffadmin.super_admin')]);
            }
            $totalFiltered = $roles->count();
        }

        $data = [];
        $userCanEdit = auth()->user()->can('edit', 'role');
        $userCanDelete = auth()->user()->can('delete', 'role');
        foreach ($roles as $role) {
            $nestedData['name'] = $role->name;
            $nestedData['display_name'] = $role->display_name;

            $editAction = '<a href="'.route('ffadmin.role.edit', $role->id).'" class="btn btn-info btn-sm" title="'.__('ffadmin::page.edit_btn').'"><i style="margin-right: 0" class="fa fa-edit"></i></a> ';
            $deleteAction = '<a href="javascript:void(0)" data-url="'.route('ffadmin.role.destroy', $role->id).'" class="btn btn-danger btn-sm delete" title="'.__('ffadmin::page.delete_btn').'"><i style="margin-right: 0" class="fa fa-trash"></i></a>';

            $nestedData['actions'] = $userCanEdit ? $editAction : '';
            $nestedData['actions'] .= $userCanDelete ? $deleteAction : '';
            $data[] = $nestedData;
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $data
        ]);
    }

    public function show (Request $request)
    {
        $this->authorize('add', 'role');

        $permissions = Permission::all();
        $permissionGroup = $this->getPermissionGroup();
        return view('ffadmin::roles.edit-add', compact('permissionGroup'));
    }

    public function edit (Request $request, $id)
    {
        $this->authorize('edit', 'role');

        $role = Role::findOrFail($id);
        $ownPermissions = $role->permissions;
        $permissionGroup = $this->getPermissionGroup();
        return view('ffadmin::roles.edit-add', compact(['role', 'ownPermissions', 'permissionGroup']));
    }

    public function store (Request $request)
    {
        $this->authorize('add', 'role');

        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'required',
        ], [
            'name.required' => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.role_col_name')]),
            'display_name.required' => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.role_field_display_name')]),
        ]);

        try {
            DB::beginTransaction();

            $role = new Role();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->save();
            $role->permissions()->attach($request->input('permissions'));

            DB::commit();
            return redirect()->route("ffadmin.role.index")->with([
                'message' => __('ffadmin::message.form_add_success'),
                'alert-type' => 'success',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with([
                'message' => __('ffadmin::message.form_add_failure'),
                'alert-type' => 'danger',
            ]);
        }
    }

    public function update (Request $request, $roleId)
    {
        $this->authorize('edit', 'role');

        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'required',
        ], [
            'name.required' => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.role_col_name')]),
            'display_name.required' => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.role_col_display_name')]),
        ]);

        try {
            DB::beginTransaction();

            $role = Role::findOrFail($roleId);
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->save();
            $role->permissions()->sync($request->input('permissions'));

            DB::commit();
            return redirect()->route("ffadmin.role.index")->with([
                'message' => __('ffadmin::message.form_update_success'),
                'alert-type' => 'success',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with([
                'message' => __('ffadmin::message.form_update_failure'),
                'alert-type' => 'danger',
            ]);
        }
    }

    public function destroy (Request $request, $roleId)
    {
        $this->authorize('delete', 'role');

        try {
            $role = Role::findOrFail($roleId);
            $role->delete();
            return $this->response(__('ffadmin::message.alert_delete_success'));
        } catch (\Exception $e) {
            return $this->response($e);
        }
    }

    private function getPermissionGroup ()
    {
        $permissions = Permission::all();
        $permissionGroup = [];
        foreach ($permissions as $permission) {
            $parts = explode('_', $permission->key);
            $group = $parts[0];
            $key = $parts[1];
            if (!isset($permissionGroup[$group])) {
                $permissionGroup[$group] = [];
            }
            $permissionGroup[$group][$key] = $permission;
        }
        return $permissionGroup;
    }
}