<?php

namespace KevinKao\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function index ()
    {
        return view('ffadmin::login');
    }

    public function logout (Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('ffadmin.login');
    }

    public function redirectTo()
    {
        return route('ffadmin.dashboard');
    }
}