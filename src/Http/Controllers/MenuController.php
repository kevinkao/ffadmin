<?php

namespace KevinKao\Admin\Http\Controllers;

use Illuminate\Http\Request;
use KevinKao\Admin\Models\MenuItem;

class MenuController extends Controller
{
    public function index ()
    {
        $this->authorize('browse', 'menus');

        $menuItems = MenuItem::orderBy('order')->get();
        return view('ffadmin::menus.index', compact('menuItems'));
    }

    public function show (Request $request, $itemId)
    {
        $this->authorize('edit', 'menus');

        $menuItem = MenuItem::findOrFail($itemId);
        $menuItem->update_url = route('ffadmin.menus.update', $itemId);
        return $menuItem;
    }

    public function create ()
    {
        $this->authorize('add', 'menus');

        return view('ffadmin::menus.edit-add');
    }

    public function store (Request $request)
    {
        $this->authorize('add', 'menus');

        $this->validate($request, [
            'title' => 'required',
        ], [
            'title.required' => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.menu_col_title')]),
        ]);

        $menuItem = new MenuItem();
        $menuItem->order = 99;
        $menuItem->fill($request->all());
        $menuItem->save();
        return $this->response(__('ffadmin::message.form_add_success'));
    }

    public function update (Request $request, $id)
    {
        $this->authorize('edit', 'menus');

        $menuItem = MenuItem::findOrFail($id);
        $menuItem->fill($request->all());
        $menuItem->save();
        return $this->response(__('ffadmin::message.form_update_success'));
    }

    public function destroy (Request $request, $id)
    {
        $this->authorize('delete', 'menus');

        $menuItem = MenuItem::findOrFail($id);
        $menuItem->delete();
        return $this->response(__('ffadmin::message.alert_delete_success'));
    }
}