<?php

namespace KevinKao\Admin\Http\Controllers;

use Illuminate\Http\Request;
use KevinKao\Admin\Models\Settings;
use DB;
use Storage;
use Log;

class SettingsController extends Controller
{
    protected $availableType = [
        'text',
        'textarea',
        'datetime_picker',
        'select',
        'input_radio',
        'image_upload',
        'editor',
    ];

    public function index ()
    {
        $this->authorize('browse', 'settings');

        $settings = Settings::all()->groupBy('group');
        $availableType = $this->availableType;
        return view('ffadmin::settings.index', compact('settings', 'availableType'));
    }

    public function store (Request $request)
    {
        $this->authorize('add', 'settings');

        $settings = new Settings();
        $settings->key = strtolower($request->input('group') . '.' . $request->input('key'));
        $settings->value = "";
        $settings->display_name = $request->input('display_name');
        $settings->type = $request->input('type');
        $settings->group = $request->input('group');
        $settings->details = $request->input('details');
        $settings->save();

        return $this->response(__('ffadmin::message.form_add_success'));
    }

    public function updateAll (Request $request)
    {
        $this->authorize('edit', 'settings');

        try {
            DB::beginTransaction();
            foreach ($request->except('_token') as $name => $value) {
                $parts = explode('_', $name);
                $group = array_shift($parts);
                $key = implode('_', $parts);
                $settings = Settings::where('key', "{$group}.{$key}")->first();

                if (empty($settings)) {
                    continue;
                }
                switch ($settings->type) {
                    case 'image_upload':
                        $uploadImage = $request->file($name);
                        $filename = md5(time()) . '.' . $uploadImage->extension();
                        $uploadImage->storeAs('settings', $filename, 'public');
                        // Remove old image before
                        Storage::disk('public')->delete($settings->value);
                        // Store new image path
                        $settings->value = "settings/{$filename}";
                        break;

                    default:
                        $settings->value = trim($value);
                }
                $settings->save();
            }
            DB::commit();
            return $this->responseBack(__('ffadmin::message.form_update_success'));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseBack($e);
        }
    }

    public function destroy (Request $request, $id)
    {
        $this->authorize('delete', 'settings');

        $settings = Settings::findOrFail($id);
        $settings->delete();
        return $this->response(__('ffadmin::message.alert_delete_success'));
    }
}