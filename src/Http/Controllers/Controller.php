<?php

namespace KevinKao\Admin\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Session;

abstract class Controller extends BaseController
{
    use ValidatesRequests, DispatchesJobs, AuthorizesRequests;

    protected function responseBack ($message, $code = 0, $alertType = 'success')
    {
        return redirect()->back()->with($this->response($message, $code, $alertType));
    }

    protected function response ($message, $code = 0, $alertType = 'success')
    {
        if ($message instanceof \Exception) {
            $message = $message->getMessage();
            $code = isset($code) ? $code : $message->getCode();
            $alertType = 'warning';
        }
        Session::flash('message', $message);
        Session::flash('alert-type', $alertType);
        return [
            'code' => $code,
            'message' => $message,
            'alert-type' => $alertType,
        ];
    }
}