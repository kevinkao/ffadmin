<?php

namespace KevinKao\Admin\Http\Controllers;

use Illuminate\Http\Request;
use KevinKao\Admin\Models\User;
use KevinKao\Admin\Models\Role;
use DB;

class UserController extends Controller
{
    protected $availableLocale = ['zh_TW', 'zh_CN'];

    public function index (Request $request)
    {
        $this->authorize('browse', 'users');

        return view('ffadmin::users.index');
    }

    public function dataTable (Request $request)
    {
        $this->authorize('browse', 'users');

        $columns = ['name', 'email', 'created_at'];

        $totalRows = User::count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $users = User::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
        } else {
            $search = $request->input('search.value');

            $users = User::where('name', 'LIKE', "%{$search}%")
                            ->orWhere('email', 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = User::where('name', 'LIKE', "%{$search}%")
                                    ->orWhere('email', 'LIKE', "%{$search}%")
                                    ->count();
        }

        $data = [];
        $userCanEdit = auth()->user()->can('edit', 'users');
        $userCanDelete = auth()->user()->can('delete', 'users');
        foreach ($users as $user) {
            $nestedData['name'] = $user->name;
            $nestedData['email'] = $user->email;
            $nestedData['created_at'] = $user->created_at;

            $editAction = '<a href="'.route('ffadmin.users.edit', $user->id).'" class="btn btn-info btn-sm" title="'.__('ffadmin::page.edit_btn').'"><i style="margin-right: 0" class="fa fa-edit"></i></a> ';
            $deleteAction = '<a href="javascript:void(0)" data-url="'.route('ffadmin.users.destroy', $user->id).'" class="btn btn-danger btn-sm delete" title="'.__('ffadmin::page.delete_btn').'"><i style="margin-right: 0" class="fa fa-trash"></i></a>';

            $nestedData['actions'] = $userCanEdit ? $editAction : '';
            $nestedData['actions'] .= $userCanDelete ? $deleteAction : '';
            $data[] = $nestedData;
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $data
        ]);
    }

    public function edit (Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize('edit', $user);

        $locale = $this->availableLocale;
        $roles = Role::exceptSuperAdmin()->get();
        return view('ffadmin::users.edit-add', compact('user', 'roles', 'locale'));
    }

    public function create ()
    {
        $this->authorize('add', 'users');

        $locale = $this->availableLocale;
        $roles = Role::exceptSuperAdmin()->get();
        return view('ffadmin::users.edit-add', compact('roles', 'locale'));
    }

    public function store (Request $request)
    {
        $this->authorize('add', 'users');

        $this->validate($request, [
            'name'             => 'required',
            'email'            => 'required|email',
            'password'         => 'required|confirmed',
            'password_confirmation' => 'required',
        ], [
            'name.required'                  => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.users_col_name')]),
            'email.required'                 => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.users_col_email')]),
            'password.required'              => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.users_col_password')]),
            'password.confirmed'             => __('ffadmin::message.form_field_password_different'),
            'password_confirmation.required' => __('ffadmin::message.form_field_required', ['field' => __(' ffadmin::page.users_col_password_confirmation')]),
        ]);

        try {
            DB::beginTransaction();

            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->save();
            $user->roles()->attach($request->input('role_select'));

            DB::commit();
            return redirect()->route("ffadmin.users.index")->with([
                'message' => __('ffadmin::message.form_add_success'),
                'alert-type' => 'success',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with([
                'message'    => __('ffadmin::message.form_add_failure'),
                'detail'     => $e->getMessage(),
                'alert-type' => 'danger',
            ]);
        }
    }

    public function update (Request $request, $id)
    {
        $this->authorize('edit', 'users');

        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'required|email',
            'password' => 'confirmed',
        ], [
            'name.required'         => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.users_col_name')]),
            'email.required'        => __('ffadmin::message.form_field_required', ['field' => __('ffadmin::page.users_col_email')]),
            'password.confirmed'    => __('ffadmin::message.form_field_password_different'),
            'password_confirmation' => __('ffadmin::message.form_field_required', ['field' => __(' ffadmin::page.users_col_password_confirmation')]),
        ]);

        try {
            DB::beginTransaction();
            $user = User::findOrFail($id);
            if ($request->input('password') !== null && $request->input('password_confirmation') !== null) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->fill($request->all());
            $user->save();
            $user->roles()->sync($request->input('role_select'));

            DB::commit();
            return redirect()->route("ffadmin.users.index")->with([
                'message' => __('ffadmin::message.form_update_success'),
                'alert-type' => 'success',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with([
                'message'    => __('ffadmin::message.form_update_failure'),
                'detail'     => $e->getMessage(),
                'alert-type' => 'danger',
            ]);
        }
    }

    public function destroy (Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('delete', $user);
            $user->delete();
            return $this->response(__('ffadmin::message.alert_delete_success'));
        } catch (\Exception $e) {
            return $this->response($e);
        }
    }
}