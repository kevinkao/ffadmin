<?php

namespace KevinKao\Admin\Http\Middleware;

use Closure;
use DB;
use Illuminate\Support\Facades\Auth;

/**
* 檢驗流水資格
*/
class BetFlowMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $class = config('betflow.model');
        $model = new $class;
        try {
            switch (config('betflow.cycle')) {
                case 'duration':
                    $duration = config('betflow.cycle_duration');
                    $result = $model
                        ->select(DB::raw('SUM(bet_total) as total'))
                        ->where('member', $request->input(config('betflow.input_field_name')))
                        ->whereBetween('data_date', [$duration[0], $duration[1]])
                        ->havingRaw('SUM(bet_total) > ?', [config('betflow.enough_amount')])
                        ->groupBy('member')
                        ->first();

                    if (empty($result)) {
                        throw new \Exception(__('ffadmin::message.betflow_not_qualified'));
                    }
                    break;

                default:
                    throw new \Exception(__('ffadmin::message.betflow_invalid_settings'));
            }
            return $next($request);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }
}