<?php

namespace KevinKao\Admin\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App;

class FFAdminLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        App::setlocale(auth()->user()->locale);
        return $next($request);
    }
}