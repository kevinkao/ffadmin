<?php

namespace KevinKao\Admin\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FFAdminAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('ffadmin.login');
        }
        return $next($request);
    }
}