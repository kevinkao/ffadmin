<?php

namespace KevinKao\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $fillable = ['title', 'url', 'icon_class', 'route'];
}