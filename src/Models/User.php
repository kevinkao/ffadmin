<?php

namespace KevinKao\Admin\Models;

use KevinKao\Admin\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    protected $fillable = ['name', 'locale'];

    public function roles ()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function getCreatedAtAttribute($value)
    {
        if (isset($value)) {
            return Carbon::parse($value)->format('Y-m-d H:i:s');
        }
        return $value;
    }

    public function isSuperAdmin ()
    {
        if ($this->roles->contains('name', config('ffadmin.super_admin'))) {
            return true;
        }
        return false;
    }
}