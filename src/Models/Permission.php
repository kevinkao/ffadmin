<?php

namespace KevinKao\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use KevinKao\Admin\Models\Role;

class Permission extends Model
{
    public function roles ()
    {
        return $this->belongsToMany(Role::class);
    }
}