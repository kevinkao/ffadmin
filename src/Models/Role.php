<?php

namespace KevinKao\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use KevinKao\Admin\Models\Permission;
use KevinKao\Admin\Models\User;

class Role extends Model
{
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users ()
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }

    public function scopeExceptSuperAdmin ($query)
    {
        if (auth()->user()->isSuperAdmin()) {
            return $query;
        }
        return $query->whereNotIn('name', [config('ffadmin.super_admin')]);
    }
}