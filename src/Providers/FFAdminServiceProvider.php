<?php

namespace KevinKao\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Schema;
use Illuminate\Contracts\Debug\ExceptionHandler;
use KevinKao\Admin\FFAdmin;
use KevinKao\Admin\Facades\FFAdminFacade;
use KevinKao\Admin\Http\Middleware\FFAdminAuthMiddleware;
use KevinKao\Admin\Http\Middleware\FFAdminLocaleMiddleware;
use KevinKao\Admin\Http\Middleware\BetFlowMiddleware;
use KevinKao\Admin\Providers\FFAdminAuthServiceProvider;
use KevinKao\Admin\Constants\FromField;
use KevinKao\Admin\FFAdminExceptionHandler;

class FFAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        Schema::defaultStringLength(191);
        
        config(['auth.providers.users.model' => \KevinKao\Admin\Models\User::class]);

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'ffadmin');
        $this->loadTranslationsFrom(realpath(__DIR__.'/../../publishable/lang'), 'ffadmin');
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../migrations'));

        $router->aliasMiddleware('admin.user', FFAdminAuthMiddleware::class);
        $router->aliasMiddleware('admin.locale', FFAdminLocaleMiddleware::class);
        $router->aliasMiddleware('admin.betflow', BetFlowMiddleware::class);
        $this->app->singleton(ExceptionHandler::class, FFAdminExceptionHandler::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(FFAdminAuthServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('FFAdmin', FFAdminFacade::class);

        $this->app->singleton('ffadmin', function () {
            return new FFAdmin();
        });

        $this->loadHelpers();
        $this->registerConfigs();

        if ($this->app->runningInConsole()) {
            $this->registerPublishableResources();
        }
    }

    public function registerPublishableResources ()
    {
        $publishablePath = dirname(__DIR__) .'/../publishable';

        $publishable = [
            'ffadmin_assets' => [
                "{$publishablePath}/assets/" => public_path(config('ffadmin.assets_path')),
            ],
            'ffadmin_seeds' => [
                "{$publishablePath}/database/seeds/" => database_path('seeds'),
            ],
            'ffadmin_config' => [
                "{$publishablePath}/config/ffadmin.php" => config_path('ffadmin.php'),
                "{$publishablePath}/config/betflow.php" => config_path('betflow.php'),
            ],

        ];

        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }

    public function loadHelpers ()
    {
        foreach (glob(__DIR__.'/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

    public function registerConfigs()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__).'/../publishable/config/ffadmin.php', 'ffadmin',
            dirname(__DIR__).'/../publishable/config/betflow.php', 'betflow'
        );
    }
}
