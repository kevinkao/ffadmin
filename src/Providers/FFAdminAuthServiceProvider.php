<?php

namespace KevinKao\Admin\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use DB;

class FFAdminAuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \KevinKao\Admin\Models\User::class => \KevinKao\Admin\Policies\UserPolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->isSuperAdmin()) {
                return true;
            }
        });

        Gate::define('add', $this->_handler('add'));
        Gate::define('delete', $this->_handler('delete'));
        Gate::define('edit', $this->_handler('edit'));
        Gate::define('browse', $this->_handler('browse'));
    }

    private function _handler ($permission)
    {
        return function ($user, $slug) use ($permission) {
            if (in_array($slug, ['dashboard'])) {
                return true;
            }

            $ownPermissions = DB::table('users')
                ->select('permissions.*')
                ->leftJoin('user_roles', 'users.id', '=', 'user_roles.user_id')
                ->leftJoin('roles', 'user_roles.role_id', '=', 'roles.id')
                ->leftJoin('permission_role', 'roles.id', '=', 'permission_role.role_id')
                ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                ->where('users.id', $user->id)
                ->get();

            $permission = "{$slug}_{$permission}";
            return $ownPermissions->contains('key', $permission);
        };
    }
}