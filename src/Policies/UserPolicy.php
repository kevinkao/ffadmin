<?php

namespace KevinKao\Admin\Policies;

use KevinKao\Admin\Models\User;

class UserPolicy
{
    public function edit ($currentUser, $user)
    {
        if ($user->isSuperAdmin()) {
            // Super admin CAN NOT edit!!
            return false;
        }
        return true;
    }

    public function delete ($currentUser, $user)
    {
        if ($user->isSuperAdmin()) {
            // Super admin CAN NOT delete!!
            return false;
        }
        return true;
    }
}