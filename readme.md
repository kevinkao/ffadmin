## Installation

Update composer.json like below example first.
``` 
"license": "MIT",
"type": "project",
"require": {
    ...
    "kevinkao/ffadmin": "2.0.1",
    ...
},
"repositories": [{
    "type": "vcs",
    "url": "https://gitlab.com/kevinkao/ffadmin.git"
}],
```

And then, update composer packages.

```
$ composer update
```

Now you already have ffadmin package! But we need to publish some files and configure to laravel structure. Let's do it below.(Of course you need to switch to laravel work folder.)
```
$ php artisan vendor:publish
 Which provider or tag's files would you like to publish?:
  [0 ] Publish files from all providers and tags listed below
  ...
  [6 ] Provider: KevinKao\Admin\Providers\FFAdminServiceProvider
  [7 ] Provider: Laravel\Tinker\TinkerServiceProvider
  [8 ] Provider: Zizaco\Entrust\EntrustServiceProvider
  [9 ] Tag: config
  [10] Tag: ffadmin_assets
  [11] Tag: ffadmin_config
  [12] Tag: ffadmin_seeds
  [13] Tag: laravel-mail
  [14] Tag: laravel-notifications
  [15] Tag: laravel-pagination
 > 6
```
Just type in 6, publish all!
```
$ php artisan migrate
```
```
$ php artisan db:seed --class FFAdminDefaultSeeder
```
Also you need to change locale in config/app.php
```
...
'locale' => 'zh_TW', // Or zh_CN is available
...
```
Update routes/web.php
```
<?php

Route::get('/', function () {
    return view('welcome');
});

FFAdmin::routeAdmin(function () {
  // You can customize your admin page route here
});

// ---- ADD THIS ROUTE INTO HOST APPLICATION ----
FFAdmin::route(function () {
  // Also you can customize your route here 
});
// ---- ADD THIS ROUTE INTO HOST APPLICATION ----
```

Default login page
```
http://you_develop_host/admin168
```

Sure, you can chnage this path in .env file(Recommend you set the path with prefix 'admin')
```
ADMIN_PREFIX=admin{Whatever}
```

Default Super Admin User
```
admin@admin.com / 123456
```